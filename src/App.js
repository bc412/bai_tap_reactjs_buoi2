import logo from './logo.svg';
import './App.css';
import Ex2_Galasses from './Components/Ex2_Galasses';

function App() {
  return (
    <div className="App">
      <Ex2_Galasses />
    </div>
  );
}

export default App;
